using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButton : MonoBehaviour
{
    public GameObject openButton;
    public GameObject botButton;
    public GameObject eventButton;
    public GameObject settingButton;
    

    public void GoToBot()
    {
        SceneManager.LoadScene(1);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
    

    public void showMenuButton()
    {
        openButton.SetActive(true);
        botButton.SetActive(true);
        eventButton.SetActive(true);
        settingButton.SetActive(true);
    }
    
    public void closeMenuButton()
    {
        openButton.SetActive(false);
        botButton.SetActive(false);
        eventButton.SetActive(false);
        settingButton.SetActive(false);
    }

}
